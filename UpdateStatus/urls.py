from django.conf.urls import url
from .views import index, update_status

#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^add_status', update_status, name='add_status'),   
]
