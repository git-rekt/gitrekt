from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Status
from .forms import Status_Form
from profile_page.models import Profile

response = {'author': "gitgud"}

def index(request):
	response['Status_Form'] = Status_Form
	status = Status.objects.all()
	profile = Profile.objects.all()
	response['status'] = status
	response['profile'] = profile
	return render(request, 'update_status.html', response)

def update_status(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		status = Status(message=response['status'])
		status.save()
		return HttpResponseRedirect('/status/')
	else:
		return HttpResponseRedirect('/status/')
