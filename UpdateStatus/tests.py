from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, update_status
from .models import Status
from .forms import Status_Form

class UpdateStatus(TestCase):
    def test_update_status_is_exist(self):
        response = Client().get('/status/')
        self.assertEqual(response.status_code,200)

    def test_update_status_using_update_status_template(self):
        response = Client().get('/status/')
        self.assertTemplateUsed(response, 'update_status.html')

    def test_update_status_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

# Create your tests here.