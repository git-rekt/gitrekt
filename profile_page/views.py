from django.shortcuts import render
from .models import Profile, Expertise

# expertise = Expertise(name='Singer')
# expertise1 = Expertise(name='Songwriter')
# expertise2 = Expertise(name='Actor')
# expertise.save()
# expertise1.save()
# expertise2.save()
# new_profile = Profile.objects.create(id=2, image='David-Bowie.jpg', name='David Bowie', birthday='1947-01-08 14:14:00', gender='Male', description='Ziggy Stardust', email='david@bowie.com')
# new_profile.expertise.add(expertise, expertise1, expertise2)

response = {}
def index(request):
    profile = Profile.objects.all()
    response['author'] = "gitgud"
    response['profile'] = profile
    return render(request, 'profile_page.html', response)

# {{ profile.date | date:"d.m.Y" }} >> bisa pake kayak gini di htmlnyaa


# Profile(name='Test', birthday=, gender='Male', description='Free rider', email='hello@gmail.com')
# profile.save()
#
# expertise1 = Expertise(name='Marketing')
# expertise2 = Expertise(name='Collector')
# expertise3 = Expertise(name='Public Speaking')
# expertise1.save()
# expertise2.save()
# expertise3.save()
#
# profile.expertise.add(expertise1)
# profile.expertise.add(expertise2)
# profile.expertise.add(expertise3)
