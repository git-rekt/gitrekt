from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Profile, Expertise

class ProfilePageUnitTest(TestCase):

    def test_profile_url_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_profile_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_profile_using_profile_page_template(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile_page.html')

    def test_model_can_create_new_message(self):
        #Creating new profile and expertise
        expertise = Expertise(name='Expertise')
        expertise1 = Expertise(name='Expertise1')
        expertise2 = Expertise(name='Expertise2')
        expertise.save()
        expertise1.save()
        expertise2.save()
        new_profile = Profile.objects.create(id=8, image='test', name='test', birthday='2017-10-12 14:14:00', gender='Male', description='Test', email='test@gmail.com')
        new_profile.expertise.add(expertise, expertise1, expertise2)
        #Retrieving all available profile and expertise
        counting_all_available_profile= Profile.objects.all().count()
        counting_all_available_expertise= Expertise.objects.all().count()
        self.assertEqual(counting_all_available_profile,2)
        self.assertEqual(counting_all_available_expertise,6)
