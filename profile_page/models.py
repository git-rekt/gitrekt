from django.db import models

class Expertise(models.Model):
    name = models.TextField()

class Profile(models.Model):
    image = models.ImageField()
    name = models.CharField(max_length=27)
    birthday = models.DateTimeField()
    gender = models.TextField()
    expertise = models.ManyToManyField(Expertise)
    description = models.TextField()
    email = models.EmailField()
