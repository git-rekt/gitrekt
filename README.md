[![pipeline status](https://gitlab.com/git-rekt/gitrekt/badges/master/pipeline.svg)](https://gitlab.com/git-rekt/gitrekt/commits/master)
[![coverage report](https://gitlab.com/git-rekt/gitrekt/badges/master/coverage.svg)](https://gitlab.com/git-rekt/gitrekt/commits/master)

Group members:
1. Rasyid Ibrahim S. 	
2. Rizki Maulana R.
3. Aghnia Prawira

Herokuapp: https://gitrekt.herokuapp.com
