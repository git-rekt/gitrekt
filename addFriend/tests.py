from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, addFriend
from .models import Friends
# from selenium import webdriver
# from selenium.webdriver.common.keys import Keys
# from selenium.webdriver.chrome.options import Options
# Create your tests here.

# class Lab5FunctionalTest(TestCase):
#
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Lab5FunctionalTest, self).setUp()
#
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Lab5FunctionalTest, self).tearDown()

    # def test_input_todo(self):
    #     selenium = self.selenium
    #     # Opening the link we want to test
    #     selenium.get('http://127.0.0.1:8000/friend/')
    #     # find the form element
    #     name = selenium.find_element_by_id('name')
    #     url = selenium.find_element_by_id('url')
    #
    #     submit = selenium.find_element_by_id('button')
    #
    #     # Fill the form with data
    #     name.send_keys('kiki')
    #     url.send_keys('herokuapp.com')
    #
    #     # submitting the form
    #     submit.send_keys(Keys.RETURN)

class profileTest(TestCase):
    def test_friendList_url_is_exist(self):
        response = Client().get('/friend/')
        self.assertEqual(response.status_code, 200)

    def test_friendList_using_index_func(self):
        found = resolve('/friend/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_message(self):
        new_activity = Friends.objects.create(name="kiki",url="google.com")
        counting_all_available_message= Friends.objects.all().count()
        self.assertEqual(counting_all_available_message,1)


    def test_friend_post_success_and_render_the_result(self):
        name = 'Anonymous'
        url = 'hai.herokuapp.com'
        response = Client().post('/friend/addFriend/', {'name': name, 'url': url})
        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn(name,html_response)
        self.assertIn(url,html_response)
