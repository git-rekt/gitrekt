from django.conf.urls import url
from .views import index, addFriend, delete_friend
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'addFriend/', addFriend, name='addFriend'),
    url(r'^delete/(?P<pk>\d+)/$', delete_friend, name='delete_friend')

]
