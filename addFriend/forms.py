from django import forms
from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .models import Friends

class Friend_Form(forms.Form):

    name = forms.CharField(label='Nama', required=True, max_length=30,)
    url = forms.URLField(label = 'URL', required=True)

    def clean(self):
        cleaned_data = super(Friend_Form, self).clean()
        name = cleaned_data.get("name")
        data = cleaned_data.get("url")
        response = Client().get(data)
        html_response = response.content.decode('utf8')
        if "herokuapp.com" not in data or response.status_code == 404:
            raise forms.ValidationError("HerokuApp does not exist")
