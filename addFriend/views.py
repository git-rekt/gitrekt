from django.shortcuts import render, redirect
from .models import Friends
from .forms import Friend_Form

def index(request):
    response = {}
    html = 'add_friend.html'
    response['Friend_Form'] = Friend_Form
    friend = Friends.objects.all()
    response['friends'] = friend
    return render(request, html , response)

def addFriend(request):
    response = {}
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friend = Friends(name=form.cleaned_data['name'], url=form.cleaned_data['url'])
        friend.save()
    else:
        response['form'] = form
    html ='add_friend.html'
    friend = Friends.objects.all()
    response['friends'] = friend
    return render(request, html, response)

def delete_friend(request, pk):
    todo = Friends.objects.filter(pk=pk).first()
    todo.delete()
    return HttpResponseRedirect('/friend/')
