from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index

class StatsPageUnitTest(TestCase):

    def test_stats_url_is_exist(self):
        response = Client().get('/stats/')
        self.assertEqual(response.status_code, 200)

    def test_stats_using_index_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, index)

    def test_stats_using_profile_page_template(self):
        response = Client().get('/stats/')
        self.assertTemplateUsed(response, 'stats.html')
