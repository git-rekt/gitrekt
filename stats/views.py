from django.shortcuts import render
from profile_page.models import Profile, Expertise
from addFriend.models import Friends
from UpdateStatus.models import Status

response = {}
def index(request):
    profile = Profile.objects.all()
    friends = Friends.objects.all().count()
    status = Status.objects.all().count()
    try:
        latest = Status.objects.latest('date')
    except Status.DoesNotExist:
        latest = None
    response['author'] = "gitgud"
    response['profile'] = profile
    response['friend'] = friends
    response['status'] = status
    response['latest'] = latest
    return render(request, 'stats.html', response)
